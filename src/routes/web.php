<?php

use Apptimus\Transaction\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => "Apptimus\Transaction\Http\Controllers"],function(){
    Route::post('test', [TransactionController::class,'saveTransaction']);
});

